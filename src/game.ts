/// --- Set up a system ---

//import {spawnGltfX, spawnEntity, spawnBoxX, spawnPlaneX} from './modules/SpawnerFunctions'
import {BuilderHUD} from './modules/BuilderHUD'



import {spawnGltfX} from './modules/SpawnerFunctions'

let d1 = new GLTFShape('models/a3/dimond.glb')
let d1Dimond = spawnGltfX(d1, 6,0,6,	0,0,0,		1,1,1)    

const scene = new Entity()
//@CF TODO i set pos != 0,0,0 to test parent effect
const scenetransform = new Transform({ position: new Vector3(0, 0, 0), rotation: Quaternion.Euler(0, 0, 0), scale: new Vector3(1, 1, 1) })
scene.addComponent(scenetransform)
engine.addEntity(scene)


class RotatorSystem {
  // this group will contain every entity that has a Transform component
  group = engine.getComponentGroup(Transform)

  update(dt: number) {
    // iterate over the entities of the group
    for (let entity of this.group.entities) {
      // get the Transform component of the entity
      const transform = entity.getComponent(Transform)

      // mutate the rotation
      transform.rotate(Vector3.Up(), dt * 10)
    }
  }
}

// Add a new instance of the system to the engine
engine.addSystem(new RotatorSystem())

/// --- Spawner function --
function spawnCube(x: number, y: number, z: number) {
  // create the entity
  const cube = new Entity()
  // add a transform to the entity
  cube.addComponent(new Transform({ position: new Vector3(x, y, z) }))
  // add a shape to the entity
  cube.addComponent(new BoxShape())
  // add the entity to the engine
  engine.addEntity(cube)
  return cube
}

/// --- Spawn a cube ---


let cyberPlugShape = new GLTFShape('models/a5/a5.glb')
let cyberPlug = spawnGltfX(cyberPlugShape, 9.3,0.68,5.1,  -180,34.031,-180,  0.02,0.007,0.02)    


const hud:BuilderHUD =  new BuilderHUD()
hud.attachToEntity(cyberPlug)
hud.setDefaultParent(scene)

/*
--------------- BuilderHUD entities -------------
preview.js:8 0,0:  Existing(9.3,0.68,5.1,  -180,34.031,-180,  0.02,0.007,0.02)
preview.js:8 0,0:  
-------------------------------------------------
*/